class ApplicationMailer < ActionMailer::Base
  default from: 'chazotflorian@gmail.com'
  layout 'mailer'
end
