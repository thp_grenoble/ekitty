class UserMailer < ApplicationMailer
default from: 'chazotflorian@gmail.com'

  def welcome_email(user)
    @user = user
     mail(to: user.email,
          subject: "Welcome to Ekitty")
   end
   def validated_cart_email(user)
     @user = user
     @order = @user.orders.last
      mail(to: @user.email,
           subject: "order validated")
    end
end
