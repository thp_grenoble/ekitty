class AdminsController < ApplicationController
  before_action :user_is_admin

  def index
    @products = Product.all
    @users = User.all
  end

  private

  def user_is_admin
    if !current_user.admin
      flash[:danger] = "Réservé aux admins !"
      redirect_to root_path
    end
  end
end
