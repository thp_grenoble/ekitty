class CartProductsController < ApplicationController
  def create
    @cart = current_user.cart
    @product = Product.find(params[:product_id])

    @cart_product = CartProduct.new(
      cart_id:    current_user.cart.id,
      product_id: params[:product_id]
    )

    if @cart_product.save
      respond_to do |format|
        format.html {
          redirect_back fallback_location: root_path,
          notice: 'Ajouté au panier !'
        }
        format.js { }
      end
    else
      respond_to do |format|
        format.html {
          redirect_back fallback_location: root_path
        }
      end
    end
  end

  def destroy
    @cart_product = CartProduct.find(params[:id])
    @product = @cart_product.product

    @cart_product.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path
                    flash[:warning] = "Produit supprimé" }
      # EZ fix for stripe button :^)
      # format.js { } 
    end
  end
end
