class UsersController < ApplicationController
  before_action :user_is_user_show, only: [:show]


  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if current_user.valid_password?(params[:user][:password])
      if @user.update(user_params)
        redirect_back(fallback_location: root_path)
        flash[:success] = "Profile updated"
      elsif @user.errors.any?
        redirect_back(fallback_location: root_path)
        flash[:danger] = []
        @user.errors.each do |attribute, message|
          flash[:danger] << "#{attribute}: #{message}"
        end
      end
    else
      redirect_back(fallback_location: root_path)
      flash[:danger] = "Invalid password"
    end
  end

  def destroy
    User.find(params[:id]).destroy
  end
  
  private

  def user_params
    params.require(:user).permit(:username, :email, :bill_address, :bill_zip, :bill_city, :bill_name, :password, :password_confirmation)
  end

  def user_is_user_show
    unless current_user == User.find(params[:id])
      flash[:danger] = "Tu ne peux pas accéder aux profils des autres utilisateurs"
      redirect_to :root
    end
  end
end
