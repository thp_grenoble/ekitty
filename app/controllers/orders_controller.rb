class OrdersController < ApplicationController
  
  def create
    order = Order.create(user: current_user)
    cart = current_user.cart
    order.fill(cart)
    current_user.cart.destroy
    Cart.create(user: current_user)

    respond_to do |format|
      format.html {
        redirect_back fallback_location: root_path
      }
    end
  end
end
