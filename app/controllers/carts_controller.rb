class CartsController < ApplicationController
  before_action :authenticate_user
  
  def create
    @product = Product.find(params[:product_id])
    @cart = Cart.new(
      product: @product,
      user: current_user,
      quantity: 1
    )

    respond_to do |format|
      if @cart.save
        format.html {
          redirect_back fallback_location: root_path,
            notice: 'Ajouté au panier !'
        }
      else
        format.html {
          redirect_back fallback_location: root_path
        }
      end
    end
  end

  def destroy
    current_user.cart.empty
    respond_to do |format|
      format.html {
        redirect_back fallback_location: root_path
      }
    end
  end

  def index
    @carts = Cart.where(user: current_user)
    @user = current_user
    #@products =
  end

  def authenticate_user
    if !user_signed_in?
      flash[:info] = "Connecte toi avant !"
      redirect_back(fallback_location: :root)
    end
  end
end
