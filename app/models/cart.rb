class Cart < ApplicationRecord
  belongs_to :user

  has_many :cart_products, dependent: :destroy
  has_many :products, through: :cart_products

  # after_create :validated_cart

  def validated_cart
    UserMailer.validated_cart_email(user).deliver_now
  end

  def empty
    CartProduct.where(cart_id: self.id).destroy_all
  end
end
