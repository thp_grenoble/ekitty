class User < ApplicationRecord
  attr_writer :login

  has_one :cart, dependent: :destroy
  has_many :cart_products, through: :cart
  has_many :carted_products, through: :cart_products, source: :product

  has_many :orders, dependent: :destroy
  has_many :order_products, through: :orders
  has_many :ordered_products, through: :order_products, source: :product

  after_create :welcome_user, :create_cart
  after_save :welcome_user

  validates :username,  presence: :true, uniqueness: { case_sensitive: false }
  validate :validate_username
  validates_confirmation_of :password


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         authentication_keys: [:login]

  # Overwrite Devise's find_for_database_authentication
  # login to accept email OR username
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  # Overwrite Devise's find_first_by_auth_conditions
  # password#new to accept email OR username
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:username)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(email: conditions[:email]).first
      end
    end
  end

  def login
    @login || self.username || self.email
  end

  def validate_username
    if User.where(email: username).exists?
      errors.add(:username, :invalid)
    end
  end

  def total_cart_price
    return self.carted_products.map{|product| product.price}.sum
  end

  def cart_product(product)
    return CartProduct.where(
      product: product,
      cart: self.cart
    ).first
  end

  def welcome_user
    UserMailer.welcome_email(self).deliver_now
  end

  def create_cart
    Cart.create!(user_id: self.id)
  end
end

# username OR email = login
#https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign-in-using-their-username-or-email-address
