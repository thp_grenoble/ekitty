class Order < ApplicationRecord
  belongs_to :user

  has_many :order_products, dependent: :destroy
  has_many :products, through: :order_products, source: :product

  def fill(cart)
    cart.products.each do |p|
      self.order_products.create(product: p)
    end
  end
end
