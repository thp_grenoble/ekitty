class Product < ApplicationRecord
  validates :name, presence: true, length: { in: 2..140 }
  validates :description, length: { in: 0..1000}
  validates :price, presence: true, numericality: { greather_than_or_equal_to: 0, less_than_or_equal_to: 10000 }
  #validates :image_url, presence: true

  has_many :cart_products, dependent: :destroy
  has_many :carts, through: :cart_products, source: :cart
  has_many :buyers, through: :carts, source: :user

  has_many :order_products, dependent: :destroy
  has_many :orders, through: :order_products, source: :order
  has_many :owners, through: :orders, source: :user

  has_one_attached :image


  def price_in_euro
    return "%0.2f" % (self.price/100.00) + "€"
  end

  def image_handler
    if self.image.attached?
      return self.image
    else
      return "missing.png"
    end
  end

  def get_cart(user)
    carts = Cart.where(user:user).first
  end

  def in_cart?(user)
    carts = CartProduct.where("cart_id = ? AND product_id = ?", user.cart.id, self)
    if carts.length == 0
      return false
    else
      return true
    end
  end

  def add_to_cart(user)
    Cart.create(
      product: self,
      user: user,
      quantity: 1
    )
  end
end
