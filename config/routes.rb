Rails.application.routes.draw do
  root to: 'products#index'

  devise_for :users
  resources :products do
    resources :carts, only: [:create, :destroy]
    resources :cart_products, only: [:create, :destroy]
  end
  resources :carts, only: [:index, :destroy]
  resources :orders, only: [:index, :create]
  resources :users, only: [:show, :update]
  resources :admins, only: [:index]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
