# e-kitty

Bienvenue sur E-Kitty, le site de vente de chats pixelisés.

## use

version prod : https://e-kitty.herokuapp.com/
on verra ca plus tard

## branches git

```mermaid
graph TD;
  MASTER-->dev;
  dev-->user_master;
  user_master-->devise;
  dev-->product_master;
  dev-->mailer_master;
  dev-->admin_master;
  dev-->kit_ui;
  dev-->stripe;
  dev-->update_db;
  dev-->imafe_hosting;
  dev-->ajaxify;
  dev-->break_stuff;
```

## la team

- Anis Bouhouche
- FLorian Chazot
- Clément Coquille
- Guillaume Silvent
- Roseline Soukamkian
