# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

CartProduct.all.destroy_all
puts "iko"
Order.all.destroy_all
puts "ok"
Product.all.destroy_all
puts "ok"
Cart.all.destroy_all
puts "ok"
User.all.destroy_all
puts "ok"

u = User.create!(
  email:          Faker::Internet.email,
  password:       'azeaze',
  bill_name:      Faker::Name.name,
  bill_address:   Faker::Address.street_address,
  bill_city:      Faker::Address.city,
  bill_zip:       Faker::Address.zip,
  username:       "azeaze"
)
10.times do
  u = User.create!(
    email:          Faker::Internet.email,
    password:       'foobar',
    bill_name:      Faker::Name.name,
    bill_address:   Faker::Address.street_address,
    bill_city:      Faker::Address.city,
    bill_zip:       Faker::Address.zip,
    username:       Faker::Name.first_name
  )
  c = Cart.create!(
    user_id:        u.id
  )
end
puts "User ok"
puts "Cart ok"

# 10.times do
#   o = Order.create!(
#     user_id:        User.all.sample.id
#   )
# end
# puts "Order oki"

5.times do
  p = Product.create!(
    name:           Faker::Games::Fallout.character,
    description:    Faker::Games::Fallout.quote,
    price:          rand(1..100),
    image_url:      Faker::Games::Fallout.location
  )
end
puts "Product ok"

# 10.times do
#   c = Cart.create!(
#     user_id:        User.all.sample.id
#   )
# end

# 100.times do
#   cp = CartProduct.create!(
#     cart_id:      Cart.all.sample.id,
#     product_id:   Product.all.sample.id
#   )
# end
# puts "CartProduct ok"

# 100.times do
#   od = OrderProduct.create!(
#     order_id:     Order.all.sample.id,
#     product_id:   Product.all.sample.id
#   )
# end
# puts "OrderProduct ok"




# 100.times do
#   o = Order.create!(
#     product_id:     Product.all.sample.id,
#     user_id:        User.all.sample.id,
#     quantity:       rand(1..10),
#     deliver:        false,
#     command_id:     rand(1..10)
#   )
# end
