class AddCommandIdToOrder < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :command_id, :integer
  end
end
