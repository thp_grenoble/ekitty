class AddBillingInfoToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :bill_name, :string
    add_column :users, :bill_address, :string
    add_column :users, :bill_city, :string
    add_column :users, :bill_zip, :integer
  end
end
